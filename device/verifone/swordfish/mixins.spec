[main]
mixinsdir: device/intel/mixins/groups

[mapping]
product.mk: device.mk

[groups]
boot-arch: efi(uefi_arch=x86_64,fastboot=efi,acpi_permissive=false,use_power_button=true,use_watchdog=true)
config-partition: enabled
kernel: gmin64(path=cht,loglevel=5)
display-density: tv
dalvik-heap: tablet-8in-tvdpi-1024
cpu-arch: slm
houdini: true
bugreport: default
graphics: ufo_gen8
storage: sdcard-mmcblk1-4xUSB-sda-emulated
ethernet: dhcp
camera: isp3
rfkill: true(force_disable=gps bluetooth)
bluetooth: rtl8723bs
nfc: none
wlan: rtl8723bs
widi: gen
audio: cht-tlv320aic31xx
media: ufo
usb: host+acc
usb-gadget: g_android
touch: atmel1000
navigationbar: true
device-type: tablet
gms: true
aware: ish
debug-tools: true
factory-scripts: true
sepolicy: intel
charger: dollar_cove_ti
disk-bus: mmc-cht
thermal: ituxd(modem_zone=false)
gps: none
serialport: ttyS0
flashfiles: true
debug-logs: true
debug-crashlogd: true
debug-coredump: true
debug-phonedoctor: true
debug-charging: true
debug-mpm: true
memory: mem-mid
hwui_cache: phone-hdpi-1024-cache
swap: zram
hdmi_settings: true
lights: true
security: txei
hw-keystore: txei
hdcpd: true
power: true
bcu: true
silentlake: true
libmds: mds
vpp: isv
libhealthd: intel
ccf: enable
sensor-hubs: ish(sensors=accelerometer compass gyroscope light proximity stepcounter stepdetector)
initrc-aosp: override
wov: lpal
widevine: true
debug-lmdump:true
pmic: dollar_cove_ti
intel_prop: true
fota: true
efiprop: unused
libmintel: true
pstore: ram_pram
