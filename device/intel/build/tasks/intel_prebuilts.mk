ifneq ($(TARGET_OUT_prebuilts),)
intel_prebuilts_top_makefile := $(TARGET_OUT_prebuilts)/Android.mk
$(intel_prebuilts_top_makefile):
	@mkdir -p $(dir $@)
	@echo 'LOCAL_PATH := $$(call my-dir)' > $@
	@echo 'ifeq ($$(TARGET_ARCH),$(TARGET_ARCH))' >> $@
	@echo 'include $$(call all-makefiles-under,$$(LOCAL_PATH))' >> $@
	@echo 'endif' >> $@

intel_prebuilts_target_makefile := $(TARGET_OUT_prebuilts)/$(TARGET_PRODUCT)/Android.mk
$(intel_prebuilts_target_makefile):
	@mkdir -p $(dir $@)
	@echo 'ifeq ($$(TARGET_PRODUCT),$(TARGET_PRODUCT))' > $@
	@echo 'LOCAL_PATH := $$(call my-dir)' >> $@
	@echo 'include $$(shell find $$(LOCAL_PATH) -mindepth 2 -name "Android.mk")' >> $@
	@echo 'endif' >> $@
endif

.PHONY: intel_prebuilts publish_intel_prebuilts generate_intel_prebuilts
intel_prebuilts: $(filter-out intel_prebuilts, $(MAKECMDGOALS))
	@$(MAKE) publish_intel_prebuilts

publish_intel_prebuilts: generate_intel_prebuilts

generate_intel_prebuilts: $(intel_prebuilts_top_makefile) $(intel_prebuilts_target_makefile)
	@$(if $(TARGET_OUT_prebuilts), \
		echo did make following prebuilts Android.mk: \
		$(foreach m, $?,\
			echo "    " $(m);) \
		find $(TARGET_OUT_prebuilts) -name Android.mk -print -exec cat {} \;)


PUB_INTEL_PREBUILTS := pub/$(TARGET_PRODUCT)/prebuilts.zip

EXTERNAL_CUSTOMER ?= "g"

INTEL_PREBUILTS_LIST := $(shell repo forall -g bsp-priv -a $(EXTERNAL_CUSTOMER)_external=bin -c 'echo $$REPO_PATH' 2>/dev/null)
INTEL_PREBUILTS_LIST := $(foreach prj, $(INTEL_PREBUILTS_LIST), $(call intel-prebuilts-path, $(prj)))
INTEL_PREBUILTS_LIST += prebuilts/intel/Android.mk
INTEL_PREBUILTS_LIST += prebuilts/intel/$(TARGET_PRODUCT)/Android.mk

$(PUB_INTEL_PREBUILTS): generate_intel_prebuilts
	@echo "Publish prebuilts for external release"
	$(hide) mkdir -p $(dir $@)
	$(hide) rm -f $@
	$(hide) cd $(PRODUCT_OUT) && zip -r $(abspath $@) $(INTEL_PREBUILTS_LIST)

# publish external if buildbot set EXTERNAL_BINARIES env variable
# and only for userdebug
ifeq (userdebug,$(TARGET_BUILD_VARIANT))
ifeq ($(EXTERNAL_BINARIES),true)
publish_intel_prebuilts: $(PUB_INTEL_PREBUILTS)
endif
endif
