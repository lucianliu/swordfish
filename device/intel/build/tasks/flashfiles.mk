ifeq ($(USE_INTEL_FLASHFILES),true)

name := $(TARGET_PRODUCT)
ifeq ($(TARGET_BUILD_TYPE),debug)
  name := $(name)_debug
endif
name := $(name)-flashfiles-$(FILE_NAME_TAG)
INTEL_FACTORY_FLASHFILES_TARGET := $(PRODUCT_OUT)/$(name).zip

ifneq ($(FLASHFILE_VARIANTS),)
INTEL_FACTORY_FLASHFILES_TARGET :=
$(foreach var,$(FLASHFILE_VARIANTS), \
	$(info Adding $(var)) \
	$(eval var_zip := $(OUT)/$(TARGET_PRODUCT)-flashfiles-$(var)-$(FILE_NAME_TAG).zip) \
	$(eval INTEL_FACTORY_FLASHFILES_TARGET += $(var_zip)) \
	$(call dist-for-goals,droidcore,$(var_zip):$(TARGET_PRODUCT)-flashfiles-$(var)-$(FILE_NAME_TAG).zip))
else
$(call dist-for-goals,droidcore,$(INTEL_FACTORY_FLASHFILES_TARGET))
endif

fftf := device/intel/build/releasetools/flashfiles_from_target_files

ifneq ($(FLASHFILE_VARIANTS),)
$(INTEL_FACTORY_FLASHFILES_TARGET): $(BUILT_TARGET_FILES_PACKAGE) $(fftf) $(MKDOSFS) $(MCOPY)
	$(hide) mkdir -p $(dir $@)
	$(eval y = $(subst -, ,$(basename $(@F))))
	$(eval DEV = $(word 3, $(y)))
	$(hide) $(fftf) --file-path=$(OUT) --device=$(DEV) $(BUILT_TARGET_FILES_PACKAGE) $@
else
$(INTEL_FACTORY_FLASHFILES_TARGET): $(BUILT_TARGET_FILES_PACKAGE) $(fftf) $(MKDOSFS) $(MCOPY)
	$(hide) mkdir -p $(dir $@)
	$(hide) $(fftf) $(BUILT_TARGET_FILES_PACKAGE) $@
endif


.PHONY: flashfiles
flashfiles: $(INTEL_FACTORY_FLASHFILES_TARGET)

ifneq ($(EFI_IFWI_BIN),)
IFWI_NAME := $(shell readlink $(EFI_IFWI_BIN))
ifneq ($(IFWI_NAME),)
INTEL_FACTORY_IFWI_TARGET := $(PRODUCT_OUT)/$(IFWI_NAME)
else
INTEL_FACTORY_IFWI_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT)_ifwi.bin
endif
endif

ifneq ($(IFWI_x32_BIN),)
IFWI_x32_NAME := $(shell readlink $(IFWI_x32_BIN))
ifneq ($(IFWI_x32_NAME),)
INTEL_FACTORY_IFWI_x32_TARGET := $(PRODUCT_OUT)/$(IFWI_x32_NAME)
else
INTEL_FACTORY_IFWI_x32_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT)_x32_ifwi.bin
endif
endif

ifneq ($(EFI_AFU_BIN),)
AFU_NAME := $(shell readlink $(EFI_AFU_BIN))
ifneq ($(AFU_NAME),)
INTEL_FACTORY_AFU_TARGET := $(PRODUCT_OUT)/$(AFU_NAME)
else
INTEL_FACTORY_AFU_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT)_afu.bin
endif
endif

ifneq ($(AFU_x32_BIN),)
AFU_x32_NAME := $(shell readlink $(AFU_x32_BIN))
ifneq ($(AFU_x32_NAME),)
INTEL_FACTORY_AFU_x32_TARGET := $(PRODUCT_OUT)/$(AFU_x32_NAME)
else
INTEL_FACTORY_AFU_x32_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT)_x32_afu.bin
endif
endif

ifneq ($(BOARD_SFU_UPDATE),)
CAPSULE_NAME := $(shell readlink $(BOARD_SFU_UPDATE))
ifneq ($(CAPSULE_NAME),)
INTEL_FACTORY_CAPSULE_TARGET := $(PRODUCT_OUT)/$(CAPSULE_NAME)
else
INTEL_FACTORY_CAPSULE_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT).fv
endif
endif

ifneq ($(EFI_EMMC_BIN),)
EMMC_NAME := $(shell readlink $(EFI_EMMC_BIN))
ifneq ($(EMMC_NAME),)
INTEL_FACTORY_EMMC_TARGET := $(PRODUCT_OUT)/$(EMMC_NAME)
else
INTEL_FACTORY_EMMC_TARGET := $(PRODUCT_OUT)/$(TARGET_PRODUCT)_emmc.bin
endif
endif

ifneq ($(EFI_IFWI_BIN),)
$(INTEL_FACTORY_IFWI_TARGET): $(EFI_IFWI_BIN) $(ACP)
	$(hide) $(ACP) $(EFI_IFWI_BIN) $(INTEL_FACTORY_IFWI_TARGET)
endif

ifneq ($(IFWI_x32_BIN),)
$(INTEL_FACTORY_IFWI_x32_TARGET): $(IFWI_x32_BIN) $(ACP)
	$(hide) $(ACP) $(IFWI_x32_BIN) $(INTEL_FACTORY_IFWI_x32_TARGET)
endif

ifneq ($(EFI_AFU_BIN),)
$(INTEL_FACTORY_AFU_TARGET): $(EFI_AFU_BIN) $(ACP)
	$(hide) $(ACP) $(EFI_AFU_BIN) $(INTEL_FACTORY_AFU_TARGET)
endif

ifneq ($(AFU_x32_BIN),)
$(INTEL_FACTORY_AFU_x32_TARGET): $(AFU_x32_BIN) $(ACP)
	$(hide) $(ACP) $(AFU_x32_BIN) $(INTEL_FACTORY_AFU_x32_TARGET)
endif

ifneq ($(BOARD_SFU_UPDATE),)
$(INTEL_FACTORY_CAPSULE_TARGET): $(BOARD_SFU_UPDATE) $(ACP)
	$(hide) $(ACP) $(BOARD_SFU_UPDATE) $(INTEL_FACTORY_CAPSULE_TARGET)
endif

ifneq ($(EFI_EMMC_BIN),)
$(INTEL_FACTORY_EMMC_TARGET): $(EFI_EMMC_BIN) $(ACP)
	$(hide) $(ACP) $(EFI_EMMC_BIN) $(INTEL_FACTORY_EMMC_TARGET)
endif

.PHONY: ifwi
ifwi: $(INTEL_FACTORY_IFWI_TARGET) $(INTEL_FACTORY_IFWI_x32_TARGET) $(INTEL_FACTORY_CAPSULE_TARGET) $(INTEL_FACTORY_EMMC_TARGET) $(INTEL_FACTORY_AFU_TARGET) $(INTEL_FACTORY_AFU_x32_TARGET) $(ACP)

endif
