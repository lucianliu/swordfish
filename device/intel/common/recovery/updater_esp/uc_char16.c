/*
 * Copyright 2015 Intel Corporation
 *
 * Author: Anisha Dattatraya Kulkarni <anisha.dattatraya.kulkarni@intel.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <uc_char16.h>

size_t uc_char16_strlen(const uc_char16_t *string)
{
    size_t len = 0;

    if (!string || !*string)
        return 0;

    while (string[len])
        len++;

    return len;
}

size_t uc_char16_strsize(const uc_char16_t *string)
{
    size_t string_len;
    size_t string_size;

    string_len = uc_char16_strlen(string);
    string_size = (string_len + 1) * sizeof(uc_char16_t);
    return string_size;
}

size_t convert_ucchar16_to_char(char *destination, const uc_char16_t *source, size_t dest_len)
{
    size_t i;
    size_t src_len;
    size_t min_len;

    src_len = uc_char16_strlen(source);
    min_len = (src_len < dest_len - 1) ? src_len : dest_len - 1;
    for (i = 0; i < min_len; i++)
        destination[i] = source[i];

    destination[i] = '\0';
    return i;
}

size_t convert_char_to_ucchar16(uc_char16_t *destination, const char *source, size_t dest_len)
{
    size_t i;
    size_t src_len;
    size_t min_len;

    src_len = strlen(source);
    min_len = (src_len < dest_len - 1) ? src_len : dest_len - 1;
    for (i = 0; i < min_len; i++)
        destination[i] = source[i];

    destination[i] = 0;
    return i;
}
