/*
 * Copyright 2015 Intel Corporation
 *
 * Author: Anisha Dattatraya Kulkarni <anisha.dattatraya.kulkarni@intel.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UPDATE_BOOTENTRY_LOADER_H
#define UPDATE_BOOTENTRY_LOADER_H

/* Search the EFI Boot Manager table, updating an entry corresponding to the
 * given label with the new path to the bootloader that should be started
 * in the EFI System Partition */
int update_bootmanager_table(const char *label, const char *loader);


#endif
