/*
 * Copyright 2015 Intel Corporation
 *
 * Author: Anisha Dattatraya Kulkarni <anisha.dattatraya.kulkarni@intel.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UC_CHAR16_H
#define UC_CHAR16_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

/* Unicode Char */
typedef uint16_t uc_char16_t;

/*
 *   Parameters passed : Unicode String
 *   Return            : size_t : The number of bytes used by a unicode character string including the null terminator
 */
size_t uc_char16_strsize(const uc_char16_t *string);

/*
 *   Parameters passed : Unicode String
 *   Return            : size_t : The length of the Unicode string not including the null terminator
 */
size_t uc_char16_strlen(const uc_char16_t *string);

/*
 *   Parameters passed : Destination Character String returned by convert_ucchar16_to_char
 *                       Source Unicode Char string
 *                       Length of the Destination buffer (*destination)
 *   Return            : size_t : The number of Unicode characters copied successfully.
 */
size_t convert_ucchar16_to_char(char *destination, const uc_char16_t *source, size_t dest_len);

/*
 *   Parameters passed : Destination Unicode Char String returned by convert_char_to_ucchar16
 *                       Source char string
 *                       Length of the Destination buffer (*destination)
 *   Return            : size_t : The number of char copied successfully.
 */
size_t convert_char_to_ucchar16(uc_char16_t *destination, const char *source, size_t dest_len);

#endif
