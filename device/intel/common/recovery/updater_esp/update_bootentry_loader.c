/*
 * Copyright 2015 Intel Corporation
 *
 * Author: Anisha Dattatraya Kulkarni <anisha.dattatraya.kulkarni@intel.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <efivar.h>
#include <uc_char16.h>
#include <update_bootentry_loader.h>

/* Each Load option Entry resides in a BootXXXX EFI variable where the XXXX is replaced by a unique option number in printable hexadecimal
   representation using the digits 0–9, and the upper case versions of the characters A–F (0000–FFFF). The XXXX must always be four digits, so small numbers
   must use leading zeros.  The below #defines - BOOT_VAR, BOOT_VAR_SIZE and BOOT_ENTRY_SIZE are used to access load option entries */

#define BOOT_VAR               "Boot"
#define BOOT_VAR_SIZE          5
#define BOOT_ENTRY_SIZE        9
#define BOOTORDER_VAR          "BootOrder"
#define MEDIA_DEVICE_TYPE      0x4
#define HARDDRIVE_DEVICE_TYPE  0x1
#define FILE_PATH_DEVICE_TYPE  0x4
#define END_DEVICE_PATH_TYPE   0x7F
#define END_ENTIRE_DEV_PATH    0xFF
#define MAXLEN                 64

typedef struct {
        uint8_t  type;
        uint8_t  subtype;
        uint16_t length;
        uint8_t  data[1];
} __attribute__((packed)) EFI_DEVICE_PATH;

typedef struct {
        uint32_t attributes;
        uint16_t file_path_list_length;
        uc_char16_t description[1];
        EFI_DEVICE_PATH file_path_list[1];
} __attribute__((packed)) EFI_LOAD_OPTION;

typedef struct {
        uint8_t type;
        uint8_t subtype;
        uint16_t length;
        uint32_t part_num;
        uint64_t start;
        uint64_t size;
        uint8_t  signature[16];
        uint8_t  mbr_type;
        uint8_t  signature_type;
} __attribute__((packed)) HARDDRIVE_DEVICE_PATH;

typedef struct {
        uint8_t type;
        uint8_t subtype;
        uint16_t length;
        uc_char16_t path_name[1];
} __attribute__((packed)) FILE_PATH_DEVICE_PATH;

typedef struct {
        uint8_t  type;
        uint8_t  subtype;
        uint16_t length;
} __attribute__((packed)) END_DEVICE_PATH;

static int create_enddevicepath(void *load_opt_dest)
{
    END_DEVICE_PATH *p_endpath;

    p_endpath = (END_DEVICE_PATH*)load_opt_dest;
    memset(p_endpath, 0, sizeof(END_DEVICE_PATH));
    p_endpath->type = END_DEVICE_PATH_TYPE;
    p_endpath->subtype = END_ENTIRE_DEV_PATH;
    p_endpath->length = sizeof(END_DEVICE_PATH);
    return p_endpath->length;
}

static int copy_harddrivedevpath(void *load_opt_dest, EFI_DEVICE_PATH *old_loader_dev_path)
{
    EFI_DEVICE_PATH *cur = old_loader_dev_path;
    int len = 0;

    while (cur->type != END_DEVICE_PATH_TYPE) {
        if (cur->type == MEDIA_DEVICE_TYPE && cur->subtype == FILE_PATH_DEVICE_TYPE)
            return len;

        memcpy((char *)load_opt_dest + len, cur, cur->length);
        len += cur->length;
        cur = (EFI_DEVICE_PATH *)((char *)cur + cur->length);
    }

    /* Did not find a FILE_PATH_DEVICE, exit with error code. */
    return -1;
}

static int create_filepathdevpath(void *load_opt_dest, const uc_char16_t *file_name)
{
    FILE_PATH_DEVICE_PATH *p_filepath;
    int ret = -1;
    char buf[1024];    //Using Buf to memset variable length FILE_PATH_DEVICE_PATH structure.
    size_t file_name_len;
    size_t file_name_size;

    file_name_len = uc_char16_strlen(file_name);
    file_name_size = uc_char16_strsize(file_name);
    memset(buf, 0, sizeof(buf));
    p_filepath = (FILE_PATH_DEVICE_PATH *)buf;
    p_filepath->type = MEDIA_DEVICE_TYPE;
    p_filepath->subtype = FILE_PATH_DEVICE_TYPE;

    /* According to the EFI spec length = Length of this structure in bytes which is 4 + file_name_size. */
    p_filepath->length = 4 + file_name_size;
    memcpy(p_filepath->path_name, file_name, file_name_size);
    memcpy(load_opt_dest, buf, p_filepath->length);
    return p_filepath->length;
}

static EFI_DEVICE_PATH *get_loaderdevpath(EFI_LOAD_OPTION *load_option)
{
    int ret = -1;
    char *load_ptr;
    size_t attribute_size;
    size_t filepathlen_size;
    size_t description_size;
    size_t dev_path_offset;

    load_ptr = (char*)load_option;
    attribute_size = sizeof(load_option->attributes);
    filepathlen_size = sizeof(load_option->file_path_list_length);
    description_size = uc_char16_strsize(load_option->description);
    dev_path_offset = attribute_size + filepathlen_size + description_size;

    return (EFI_DEVICE_PATH *)(load_ptr + dev_path_offset);
}

static int get_bootentry_label(const char *boot_entryname, char *boot_label)
{
    int i, ret = -1;
    void *data = NULL;
    efi_guid_t global_guid = EFI_GLOBAL_GUID;
    EFI_LOAD_OPTION *load_option = NULL;
    size_t dsize;
    uint32_t attributes;

    ret = efi_get_variable(global_guid, boot_entryname, (uint8_t **)&data,
                       &dsize, &attributes);
    if (ret) {
        printf("get_bootentry_label: couldn't read %s\n", boot_entryname);
        return ret;
    }

    load_option = (EFI_LOAD_OPTION *)data;
    convert_ucchar16_to_char(boot_label, load_option->description, MAXLEN);
    free(data);
    return 0;
}

static int get_boot_entries(uint16_t **boot_entries, int *num_bootentries)
{
    int ret = -1 ;
    void *data = NULL;
    efi_guid_t global_guid = EFI_GLOBAL_GUID;
    size_t dsize;
    uint32_t attributes;

    ret = efi_get_variable(global_guid, BOOTORDER_VAR ,(uint8_t **)&data,
                       &dsize, &attributes);
    if (ret || dsize % 2 != 0) {
        printf("get_boot_entries: Couldn't read %s\n", BOOTORDER_VAR);
        free(data);
        return ret;
    }

    *boot_entries = (uint16_t*)data;
    *num_bootentries = dsize / sizeof(*boot_entries);
    return 0;
}

static int copy_loadoption_data(EFI_LOAD_OPTION *load_opt_new_data, EFI_LOAD_OPTION *load_opt_old_data)
{
    char *p_load_opt;
    char *start_load_opt;
    int loadoption_len = 0;

    if (!load_opt_new_data || !load_opt_old_data) {
        return -1;
    }

    p_load_opt = (char*)load_opt_new_data;
    start_load_opt = (char*)load_opt_new_data;

    /* Copy attributes and description from existing Boot entry
       Add size of Attributes and size of description to p_load_opt */
    load_opt_new_data->attributes = load_opt_old_data->attributes;
    p_load_opt += sizeof(load_opt_new_data->attributes);

    /* file_path_list_length is skipped now */
    p_load_opt += sizeof(load_opt_new_data->file_path_list_length);
    memcpy(load_opt_new_data->description, load_opt_old_data->description,
           uc_char16_strsize(load_opt_old_data->description));
    p_load_opt += uc_char16_strsize(load_opt_new_data->description);
    loadoption_len = p_load_opt - start_load_opt;
    return loadoption_len;
}

/*
 The data file of the boot entry efivar is in the below format. build_load_option copies all the data from existing
 file and modifies only the path_name. description and path_name are variable lenght fields and file_path_list_len
 and length(FILE_PATH_DEV_PATH) are needed to be modified according to description and path_name sizes.
                        ----------------------------------------------------------------------------------
   EFI_LOAD_OPTION     |        attributes              |file_path_list_len| description (variable length) |
                        ----------------------------------------------------------------------------------
                        ----------------------------------------------------------------------------------
  EFI_DEVICE_PATH*     | type |subtype|   length        |    [...]                                         |
                        ----------------------------------------------------------------------------------
                                                           [...]
                        ----------------------------------------------------------------------------------
 FILE_PATH_DEVICE_PATH | mbr_type| sig_type| type |subtyp|     length       | path_name (variable length   |
                        -----------------------------------------------------------------------------------
  END_DEVICE_PATH      |  type | subtype| length         | end \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ |
                       ------------------------------------------------------------------------------------
*/
static int build_load_option(EFI_LOAD_OPTION *old_load_option, const char *loader_name, EFI_LOAD_OPTION *load_option, int *byte_to_write)
{
    char *p_load_opt;
    char *start;
    char *q_load_opt;
    int result = 0;
    EFI_DEVICE_PATH *old_loader_dev_path;
    uc_char16_t uc_loader_name[MAXLEN];

    p_load_opt = (char*)load_option;
    start = (char*)load_option;
    result = copy_loadoption_data(load_option, old_load_option);
    if (result == -1)
        return -1;

    p_load_opt += result;
    q_load_opt = p_load_opt;
    convert_char_to_ucchar16(uc_loader_name, loader_name, MAXLEN);

    old_loader_dev_path = get_loaderdevpath(old_load_option);

    result = copy_harddrivedevpath(p_load_opt, old_loader_dev_path);
    if (result == -1)
        return -1;
    p_load_opt += result;

    result = create_filepathdevpath(p_load_opt, uc_loader_name);
    p_load_opt += result;

    result = create_enddevicepath(p_load_opt);
    p_load_opt += result;

    load_option->file_path_list_length = p_load_opt - q_load_opt;
    *byte_to_write = p_load_opt - start;

    return 0;
}

static int modify_load_option(const char *boot_entryname ,const char *loader_name)
{
    int ret = -1;
    efi_guid_t global_guid = EFI_GLOBAL_GUID;
    void *data_read = NULL;
    size_t dsize;
    uint32_t attributes;
    uint8_t vardata[1024];
    int byte_to_write;

    ret = efi_get_variable(global_guid, boot_entryname, (uint8_t **)&data_read,
                       &dsize, &attributes);
    if (ret) {
        printf("failed to read boot manager entry %s\n", boot_entryname);
        goto done;
    }

    ret = build_load_option(data_read, loader_name, (EFI_LOAD_OPTION*)vardata,
                            &byte_to_write);
    if (ret)
        goto done;

    ret = efi_del_variable(global_guid, boot_entryname);
    if (ret < 0 ) {
        printf("failed to clear %s\n", boot_entryname);
        goto done;
    }

    ret = efi_set_variable(global_guid, boot_entryname,
                              (uint8_t*)vardata, byte_to_write,
                              EFI_VARIABLE_BOOTSERVICE_ACCESS |
                              EFI_VARIABLE_RUNTIME_ACCESS |
                              EFI_VARIABLE_NON_VOLATILE);
    if (ret < 0) {
        printf("failed to update %s with %s\n", boot_entryname, loader_name);
        goto done;
    }

    ret = 0;
done:
    free(data_read);
    return ret;
}


int update_bootmanager_table(const char *label, const char *loader)
{
    uint16_t *boot_entry = NULL;
    int num_bootentry = 0;
    int ret, i;

    if (!label || !loader) {
        printf("Bad parameters\n");
        return -1;
    }

    printf("updating bootmanager table: %s --> %s\n", label, loader);

    ret = get_boot_entries(&boot_entry, &num_bootentry);
    if (ret) {
        printf("Couldn't get boot entries\n");
        return -1;
    }

    /* Linear search through the boot table for the label we want to update */
    for (i = 0; i < num_bootentry; i++) {
        char curr_label[MAXLEN];
        char entry[BOOT_ENTRY_SIZE];

        snprintf(entry, BOOT_ENTRY_SIZE, "%s%04X", BOOT_VAR, boot_entry[i]);

        ret = get_bootentry_label(entry, curr_label);
        if (ret) {
            printf("Couldn't fetch boot entry label %s", entry);
            return -1;
        }

        if (strcmp(curr_label, label) == 0)  {
            printf("%s matches entry %s\n", label, entry);
            ret = modify_load_option(entry, loader);
            if (ret) {
                printf("Couldn't modify load option %s -> %s\n", entry, loader);
                return -1;
            }
            return 0;
        }
    }

    printf("No matching table entry for %s\n", label);
    /* TODO create a new table entry */
    return -1;
}


