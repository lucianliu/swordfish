
modules=`getprop ro.modules.location`

insmod $modules/compat.ko
insmod $modules/iwl-cfg80211.ko
insmod $modules/iwl-mac80211.ko

if [ $1 == "--ptest-boot" ]; then
        insmod $modules/iwlwifi.ko nvm_file=nvmData xvt_default_mode=1
        insmod $modules/iwlxvt.ko
else
        insmod $modules/iwlwifi.ko nvm_file=nvmData d0i3_debug=1
        insmod $modules/iwlmvm.ko power_scheme=1
fi
