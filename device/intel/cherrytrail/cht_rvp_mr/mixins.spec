[main]
mixinsdir: device/intel/mixins/groups

[mapping]
product.mk: device.mk

[groups]
boot-arch: efi(uefi_arch=x86_64,fastboot=efi,acpi_permissive=false,use_power_button=true,use_watchdog=true)
config-partition: enabled
kernel: gmin64(path=cht,loglevel=5)
display-density: high
dalvik-heap: tablet-8.3in-280dpi-2048
csm: true
cellcoex: true
cpu-arch: slm
houdini: true
bugreport: default
graphics: ufo_gen8
storage: sdcard-mmcblk1-4xUSB-sda-emulated
ethernet: dhcp
camera: isp3
bluetooth: bcm43241
nfc: pn547
wlan: bcm43241
widi: gen
wfa-sigma: sigma_gen
aware: ish
audio: cht-rt5672
media: ufo
usb: host+acc
usb-gadget: g_android
touch: atmel1000
navigationbar: true
device-type: tablet
gms: true
debug-tools: true
factory-scripts: true
sepolicy: intel
charger: true
disk-bus: mmc-cht
thermal: ituxd(modem_zone=true)
gps: bcm4752(tty=ttyHSU1,RfType=GL_RF_4752_BRCM_EXT_LNA,FrqPlan=FRQ_PLAN_26MHZ_2PPM)
rfkill: true(force_disable=gps bluetooth)
serialport: ttyS0
flashfiles: true
debug-logs: true
debug-crashlogd: true
debug-coredump: true
debug-phonedoctor: true
debug-charging: true
debug-mpm: true
memory: mem-mid
hwui_cache: phone-hdpi-1024-cache
swap: zram
lights: true
security: txei
hw-keystore: txei
telephony: tablet_flashless(modems=7260_HW30_SW20,pci_id=14,bus_id=2,ssic_port=5,bus_id_hs=1,usb_port_m2=3,hsic_port=1-0:1.0/port6)
hdcpd: true
hdmi_settings: true
power: true
bcu: true
libhealthd: intel
silentlake: true
libmds: mds
vpp: isv
ccf: enable
sensor-hubs: ish(sensors=accelerometer barometer compass gyroscope light proximity stepcounter stepdetector)
initrc-aosp: override
wov: lpal
widevine: true
debug-lmdump:true
efiprop: used
telephony-config: dynamic(user_build_config=generic_nomodem)
intel_prop: true
factory-partition: enabled
fota: true
battery: dynamic
libmintel: true
pstore: ram_pram
