#!/bin/sh

# Copyright 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# start klp-dev
# 886418 = KRT16I
# end klp-dev

ZIP=cht_simics-ota
BUILD=none

ROOTDEVICE=cht_simics
DEVICE=cht_simics
MANUFACTURER=intel

# NOTE: need to match all the cases in extract-lists.txt
for COMPANY in intel_mit intel_obl intel_oblumg
do
  echo Processing files from $COMPANY
  rm -rf tmp
  FILEDIR=tmp/vendor/$COMPANY/$DEVICE/proprietary
  mkdir -p $FILEDIR
  mkdir -p tmp/vendor/$MANUFACTURER/$ROOTDEVICE
  case $COMPANY in

# SNIP - put contents of generated extract-lists.txt here

  intel_mit)
    TO_EXTRACT="\
            system/lib/libva-android.so \
            system/lib/libva.so \
            system/lib/libva-tpi.so \
            "
    ;;
  intel_obl)
    TO_EXTRACT="\
            system/bin/coreu \
            system/bin/mediainfo \
            system/etc/route_criteria.conf \
            system/lib/audio.routemanager.so \
            system/lib/egl/libGLES_intel7.so \
            system/lib/hw/camera.gmin.so \
            system/lib/hw/gralloc.gmin.so \
            system/lib/hw/hwcomposer.gmin.so \
            system/lib/hw/sensors.gmin.so \
            system/lib/i965_drv_video.so \
            system/lib/igfxcmjit32.so \
            system/lib/igfxcmrt32.so \
            system/lib/lib2d.so \
            system/lib/libcoreuclient.so \
            system/lib/libcoreuinterface.so \
            system/lib/libcoreuservice.so \
            system/lib/libgabi++-mfx.so \
            system/lib/libgmin_audio_hardwaredetection.so \
            system/lib/libgrallocclient.so \
            system/lib/libgrallocgmm.so \
            system/lib/libgsmgr.so \
            system/lib/libhwcservice.so \
            system/lib/libI420colorconvert.so \
            system/lib/libia_aiq.so \
            system/lib/libia_cmc_parser.so \
            system/lib/libiacp.so \
            system/lib/libia_dvs_2.so \
            system/lib/libia_emd_decoder.so \
            system/lib/libia_exc.so \
            system/lib/libia_face.so \
            system/lib/libia_isp_1_5.so \
            system/lib/libia_isp_2_2.so \
            system/lib/libia_log.so \
            system/lib/libia_mkn.so \
            system/lib/libia_nvm.so \
            system/lib/libia_panorama.so \
            system/lib/libintelmetadatabuffer.so \
            system/lib/libinterface-provider-lib.so \
            system/lib/libinterface-provider.so \
            system/lib/libivp.so \
            system/lib/libmdp_omx_core.so \
            system/lib/libmfxhw32.so \
            system/lib/libmfx_omx_components_hw.so \
            system/lib/libmfx_omx_components_sw.so \
            system/lib/libmfx_omx_core.so \
            system/lib/libmfxsw32.so \
            system/lib/libmix_videovpp.so \
            system/lib/libparameter.so \
            system/lib/libpavpdll.so \
            system/lib/libpcp.so \
            system/lib/libproperty.so \
            system/lib/libremote-processor.so \
            system/lib/libskuwa.so \
            system/lib/libstagefrighthw.so \
            system/lib/libstagefright_soft_aacdec_mdp.so \
            system/lib/libstagefright_soft_aacenc_mdp.so \
            system/lib/libstagefright_soft_mp3dec_mdp.so \
            system/lib/libstagefright_soft_vorbisdec_mdp.so \
            system/lib/libstlport-mfx.so \
            system/lib/libtbd.so \
            system/lib/libuevent.so \
            system/lib/parameter-framework-plugins/Audio/libtinyalsactl-subsystem.so \
            system/lib/parameter-framework-plugins/Audio/libtinyamixer-subsystem.so \
            system/lib/parameter-framework-plugins/Fs/libfs-subsystem.so \
            system/lib/parameter-framework-plugins/Route/libroute-subsystem.so \
            system/lib/soundfx/liblpepreprocessing.so \
            "
    ;;
  intel_oblumg)
    TO_EXTRACT="\
            system/bin/disable_houdini \
            system/bin/enable_houdini \
            system/bin/houdini \
            system/bin/remote-process \
            system/etc/atomisp/00gc2235.cpf \
            system/etc/atomisp/00imx134.cpf \
            system/etc/atomisp/00ov5693.cpf \
            system/etc/atomisp/01gc0339.cpf \
            system/etc/atomisp/01mt9m114.cpf \
            system/etc/atomisp/01ov2722.cpf \
            system/etc/firmware/bt/BCM43241B0_0082.hcd \
            system/etc/firmware/bt/BCM4324B3_002.004.006.0076.0093.hcd \
            system/etc/firmware/fw_sst_0f28_ssp0.bin \
            system/etc/firmware/fw_sst_0f28_ssp2.bin \
            system/etc/firmware/isp_acc_chromaproc_css21_2400b0.bin \
            system/etc/firmware/isp_acc_deghosting_v2_css21_2400b0.bin \
            system/etc/firmware/isp_acc_lumaproc_css21_2400b0.bin \
            system/etc/firmware/isp_acc_multires_v2_css21_2400b0.bin \
            system/etc/firmware/isp_acc_warping_v2_css21_2400b0.bin \
            system/etc/firmware/shisp_2400b0_v21.bin \
            system/lib/arm/.assets_lib_list \
            system/lib/arm/cpuinfo \
            system/lib/arm/cpuinfo.neon \
            system/lib/arm/libandroidfw.so \
            system/lib/arm/libandroid_runtime.so \
            system/lib/arm/libandroid.so \
            system/lib/arm/libaudioflinger.so \
            system/lib/arm/libaudioutils.so \
            system/lib/arm/libbcc.so \
            system/lib/arm/libbcinfo.so \
            system/lib/arm/libbinder.so \
            system/lib/arm/libcamera_client.so \
            system/lib/arm/libcamera_metadata.so \
            system/lib/arm/libcommon_time_client.so \
            system/lib/arm/libconnectivitymanager.so \
            system/lib/arm/libc_orig.so \
            system/lib/arm/libcorkscrew.so \
            system/lib/arm/libcrypto.so \
            system/lib/arm/libc.so \
            system/lib/arm/libcutils.so \
            system/lib/arm/libdl.so \
            system/lib/arm/libdrmframework.so \
            system/lib/arm/libdvm.so \
            system/lib/arm/libeffects.so \
            system/lib/arm/libEGL.so \
            system/lib/arm/libETC1.so \
            system/lib/arm/libexpat.so \
            system/lib/arm/libfilterfw.so \
            system/lib/arm/libfilterpack_imageproc.so \
            system/lib/arm/libft2.so \
            system/lib/arm/libgabi++.so \
            system/lib/arm/libgccdemangle.so \
            system/lib/arm/libGLESv1_CM.so \
            system/lib/arm/libGLESv2.so \
            system/lib/arm/libgui.so \
            system/lib/arm/libhardware_legacy.so \
            system/lib/arm/libhardware.so \
            system/lib/arm/libharfbuzz_ng.so \
            system/lib/arm/libharfbuzz.so \
            system/lib/arm/libhwui.so \
            system/lib/arm/libicui18n.so \
            system/lib/arm/libicuuc.so \
            system/lib/arm/libinput.so \
            system/lib/arm/libjnigraphics.so \
            system/lib/arm/libjpeg.so \
            system/lib/arm/libLLVM.so \
            system/lib/arm/liblog.so \
            system/lib/arm/libmedia.so \
            system/lib/arm/libmemtrack.so \
            system/lib/arm/libm_orig.so \
            system/lib/arm/libm.so \
            system/lib/arm/libnativehelper.so \
            system/lib/arm/libnbaio.so \
            system/lib/arm/libnetutils.so \
            system/lib/arm/libnfc_ndef.so \
            system/lib/arm/libOpenMAXAL.so \
            system/lib/arm/libOpenSLES.so \
            system/lib/arm/libpixelflinger.so \
            system/lib/arm/libpng.so \
            system/lib/arm/libpowermanager.so \
            system/lib/arm/libRScpp.so \
            system/lib/arm/libRSDriver.so \
            system/lib/arm/libRS.so \
            system/lib/arm/libselinux.so \
            system/lib/arm/libskia.so \
            system/lib/arm/libsonivox.so \
            system/lib/arm/libspeexresampler.so \
            system/lib/arm/libsqlite.so \
            system/lib/arm/libssl.so \
            system/lib/arm/libstagefright_avc_common.so \
            system/lib/arm/libstagefright_enc_common.so \
            system/lib/arm/libstagefright_foundation.so \
            system/lib/arm/libstagefright_omx.so \
            system/lib/arm/libstagefright.so \
            system/lib/arm/libstagefright_yuv.so \
            system/lib/arm/libstdc++.so \
            system/lib/arm/libstlport.so \
            system/lib/arm/libsurfaceflinger.so \
            system/lib/arm/libsync.so \
            system/lib/arm/libui.so \
            system/lib/arm/libusbhost.so \
            system/lib/arm/libutils.so \
            system/lib/arm/libvideoeditor_core.so \
            system/lib/arm/libvideoeditor_jni.so \
            system/lib/arm/libvideoeditor_osal.so \
            system/lib/arm/libvideoeditorplayer.so \
            system/lib/arm/libvideoeditor_videofilters.so \
            system/lib/arm/libvorbisidec.so \
            system/lib/arm/libwebrtc_audio_coding.so \
            system/lib/arm/libwpa_client.so \
            system/lib/arm/libz.so \
            system/lib/arm/linker \
            system/lib/hw/audio.primary.asus_t100.so \
            system/lib/libcilkrts.so \
            system/lib/libhoudini.so \
            "
    ;;

# SNIP

  esac
  echo \ \ Extracting files from OTA package
  for ONE_FILE in $TO_EXTRACT
  do
    echo \ \ \ \ Extracting $ONE_FILE
    unzip -j -o $ZIP $ONE_FILE -d $FILEDIR > /dev/null || echo \ \ \ \ Error extracting $ONE_FILE
  done
  echo \ \ Setting up $COMPANY-specific makefiles
  cp -R $COMPANY/staging/* tmp/vendor/$COMPANY/$DEVICE || echo \ \ \ \ Error copying makefiles
  echo \ \ Setting up shared makefiles
  cp -R root/* tmp/vendor/$MANUFACTURER/$ROOTDEVICE || echo \ \ \ \ Error copying makefiles
  echo \ \ Generating self-extracting script
  SCRIPT=extract-$COMPANY-$DEVICE.sh
  cat PROLOGUE > tmp/$SCRIPT || echo \ \ \ \ Error generating script
  cat $COMPANY/COPYRIGHT >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  cat PART1 >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  cat $COMPANY/LICENSE >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  cat PART2 >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  echo tail -n +$(expr 2 + $(cat PROLOGUE $COMPANY/COPYRIGHT PART1 $COMPANY/LICENSE PART2 PART3 | wc -l)) \$0 \| tar zxv >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  cat PART3 >> tmp/$SCRIPT || echo \ \ \ \ Error generating script
  (cd tmp ; tar zc --owner=root --group=root vendor/ >> $SCRIPT || echo \ \ \ \ Error generating embedded tgz)
  chmod a+x tmp/$SCRIPT || echo \ \ \ \ Error generating script
  ARCHIVE=$COMPANY-$DEVICE-$BUILD-$(md5sum < tmp/$SCRIPT | cut -b -8 | tr -d \\n).tgz
  rm -f $ARCHIVE
  echo \ \ Generating final archive
  (cd tmp ; tar --owner=root --group=root -z -c -f ../$ARCHIVE $SCRIPT || echo \ \ \ \ Error archiving script)
  rm -rf tmp
done
