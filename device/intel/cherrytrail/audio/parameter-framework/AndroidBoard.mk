PLATFORM_PFW_CONFIG_PATH := $(call my-dir)

# The following included file defines:
# -$(COMMON_PFW_CONFIG_PATH)
# - $(PFW_TUNING_ALLOWED)
# The value of PFW_TUNING_ALLOWED will be used to remplace the
# "@TUNING_ALLOWED@" pattern in top-level configuration files templates
include device/intel/common/audio/parameter-framework/AndroidBoard.mk

LOCAL_PATH := $(PLATFORM_PFW_CONFIG_PATH)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := parameter-framework.audio.cherrytrail
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES :=  \
    parameter-framework.audio.common \
    AudioParameterFramework.xml \
    ConfigurationSubsystem.xml \
    SstSubsystem \
    IMCSubsystem.xml \
    PowerSubsystem.xml \
    PropertySubsystem.xml \
    CMESubsystem.xml
include $(BUILD_PHONY_PACKAGE)

##################################################

# Audio PFW top-level configuration file
 include $(CLEAR_VARS)
LOCAL_MODULE := AudioParameterFramework.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework
LOCAL_SRC_FILES := $(LOCAL_MODULE).in

include $(BUILD_SYSTEM)/base_rules.mk

# This target automatically replaces the TUNING_ALLOWED variable with the
# corresponding boolean value according to the type of build.
$(LOCAL_BUILT_MODULE): MY_FILE := $(LOCAL_PATH)/$(LOCAL_MODULE).in
$(LOCAL_BUILT_MODULE): MY_TUNING_ALLOWED := $(PFW_TUNING_ALLOWED)
$(LOCAL_BUILT_MODULE):
	$(hide) mkdir -p $(dir $@)
	sed -e 's/@TUNING_ALLOWED@/$(MY_TUNING_ALLOWED)/' $(MY_FILE) > $@

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := parameter-framework.route.cherrytrail
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES :=  \
    parameter-framework.route.cherrytrail.nodomains \
    RouteConfigurableDomains.xml
include $(BUILD_PHONY_PACKAGE)

include $(CLEAR_VARS)

LOCAL_MODULE := parameter-framework.route.cherrytrail.nodomains
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES := \
    RouteClass-common.xml \
    RouteParameterFramework.xml \
    RouteSubsystem.xml \
    DebugFsSubsystem.xml
include $(BUILD_PHONY_PACKAGE)

##################################################

# Route PFW top-level configuration file
 include $(CLEAR_VARS)
LOCAL_MODULE := RouteParameterFramework.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework
LOCAL_SRC_FILES := $(LOCAL_MODULE).in

include $(BUILD_SYSTEM)/base_rules.mk

# This target automatically replaces the TUNING_ALLOWED variable with the
# corresponding boolean value according to the type of build.
$(LOCAL_BUILT_MODULE): MY_FILE := $(LOCAL_PATH)/$(LOCAL_MODULE).in
$(LOCAL_BUILT_MODULE): MY_TUNING_ALLOWED := $(PFW_TUNING_ALLOWED)
$(LOCAL_BUILT_MODULE):
	$(hide) mkdir -p $(dir $@)
	sed -e 's/@TUNING_ALLOWED@/$(MY_TUNING_ALLOWED)/' $(MY_FILE) > $@

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := RouteSubsystem.xml
LOCAL_MODULE_STEM := RouteSubsystem.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Route
LOCAL_REQUIRED_MODULES := \
    libroute-subsystem \
    RouteSubsystem-CommonCriteria.xml \
    RouteSubsystem-RoutesTypes.xml
ifeq ($(BOARD_USES_MODEM_CENTRIC_ARCHITECTURE),true)
    LOCAL_SRC_FILES := Structure/Route/RouteSubsystem_modem_centric.xml
else
    LOCAL_SRC_FILES := Structure/Route/$(LOCAL_MODULE)
endif

include $(BUILD_PREBUILT)

##################################################
# Generate routing domains file
include $(CLEAR_VARS)
LOCAL_MODULE := RouteConfigurableDomains.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Settings/Route
LOCAL_MODULE_STEM := RouteConfigurableDomains.xml

ifeq ($(BOARD_USES_MODEM_CENTRIC_ARCHITECTURE),true)
    ROUTES_CONF_FILE := $(LOCAL_PATH)/Settings/Route/routes_modem_centric.pfw
else
    ROUTES_CONF_FILE := $(LOCAL_PATH)/Settings/Route/routes.pfw
endif

LOCAL_ADDITIONAL_DEPENDENCIES := \
    parameter-framework.route.cherrytrail.nodomains

PFW_TOPLEVEL_FILE := $(TARGET_OUT_ETC)/parameter-framework/RouteParameterFramework.xml
PFW_CRITERIA_FILE := $(COMMON_PFW_CONFIG_PATH)/RouteCriteria.txt
PFW_EDD_FILES := \
    $(ROUTES_CONF_FILE) \
    $(COMMON_PFW_CONFIG_PATH)/Settings/Route/parameters.pfw

include $(BUILD_PFW_SETTINGS)
##################################################

##############################
# SstSubsystem Phony package
include $(CLEAR_VARS)
LOCAL_MODULE := SstSubsystem
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES :=  \
    SstSubsystem.xml \
    CommonAlgoTypes.xml \
    Gain.xml \
    VoiceVolume.xml \
    Dcr.xml \
    SbaFir.xml \
    SbaIir.xml \
    Lpro.xml \
    Mdrc.xml \
    SbaEqualizers.xml \
    ToneGenerator_V2_4.xml \
    MediaAlgos_Gen3_5 \
    VoiceAlgos_Gen3_5 \
    HfSns2.xml \
    ModuleVoiceProcessingLock_V1_0.xml
include $(BUILD_PHONY_PACKAGE)

##################################################

# Vibra
include $(CLEAR_VARS)
LOCAL_MODULE := parameter-framework.vibrator.cherrytrail
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES :=  \
    parameter-framework.vibrator.common \
    ParameterFrameworkConfigurationVibrator.xml \
    SysfsVibratorClass.xml \
    SysfsVibratorSubsystem.xml \
    MiscConfigurationSubsystem.xml \
    VibratorConfigurableDomains.xml
include $(BUILD_PHONY_PACKAGE)

##################################################

# Vibra PFW top-level configuration file
include $(CLEAR_VARS)
LOCAL_MODULE := ParameterFrameworkConfigurationVibrator.xml
LOCAL_MODULE_STEM := ParameterFrameworkConfigurationVibrator.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework
LOCAL_SRC_FILES := $(LOCAL_MODULE).in

include $(BUILD_SYSTEM)/base_rules.mk

# This target automatically replaces the TUNING_ALLOWED variable with the
# corresponding boolean value according to the type of build.
$(LOCAL_BUILT_MODULE): MY_FILE := $(LOCAL_PATH)/$(LOCAL_MODULE).in
$(LOCAL_BUILT_MODULE): MY_TUNING_ALLOWED := $(PFW_TUNING_ALLOWED)
$(LOCAL_BUILT_MODULE):
	$(hide) mkdir -p $(dir $@)
	sed -e 's/@TUNING_ALLOWED@/$(MY_TUNING_ALLOWED)/' $(MY_FILE) > $@

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := SysfsVibratorClass.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Vibrator
LOCAL_SRC_FILES := Structure/Vibrator/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := SysfsVibratorSubsystem.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Vibrator
LOCAL_SRC_FILES := Structure/Vibrator/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := VibratorConfigurableDomains.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Settings/Vibrator
LOCAL_SRC_FILES := Settings/Vibrator/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := MiscConfigurationSubsystem.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Vibrator
LOCAL_SRC_FILES := Structure/Vibrator/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################
