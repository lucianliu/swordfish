DEVICE_PFW_CONFIG_PATH := $(call my-dir)

#
# Defines all variables required to :
#    -configure mapping field of PFW plugings automatically at compile time
#    -use the specific device/codec structure/settings files.
#
SOUND_CARD_NAME := cherrytrailaud
# Currently Cherrytrail T3 & T4 have the same sound card name.
# DEVICE_SOUND_CARD_NAME will temporarily contain a generic name for platfome Cherrytrail
DEVICE_SOUND_CARD_NAME := cherrytrailaud

# The following included file is required to provides some definitions:
# - $(COMMON_PFW_CONFIG_PATH)
# - basic targets, shared by all devices, that do not require files
#   specifically in the target device's folder
# This file needs (these should be defined before this file is included):
# - DEVICE_PFW_CONFIG_PATH
# - DEVICE_SOUND_CARD_NAME
# - SOUND_CARD
include device/intel/cherrytrail/audio/parameter-framework/AndroidBoard.mk

LOCAL_PATH := $(DEVICE_PFW_CONFIG_PATH)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := parameter-framework.audio.$(TARGET_DEVICE)
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES := \
        AudioConfigurableDomains.xml
include $(BUILD_PHONY_PACKAGE)

include $(CLEAR_VARS)
LOCAL_MODULE := parameter-framework.audio.$(TARGET_DEVICE).nodomains
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES :=  \
        parameter-framework.audio.cherrytrail \
        AudioClass.xml \
        Realtek5672Subsystem.xml
include $(BUILD_PHONY_PACKAGE)

##################################################
## Audio Tuning + Routing

include $(CLEAR_VARS)
LOCAL_MODULE := AudioConfigurableDomains.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Settings/Audio

include $(CLEAR_PFW_VARS)
# Refresh tunning + routing domain file
LOCAL_ADDITIONAL_DEPENDENCIES := parameter-framework.audio.$(TARGET_DEVICE).nodomains

PFW_COPYBACK := Settings/Audio/$(LOCAL_MODULE)
PFW_TOPLEVEL_FILE := $(TARGET_OUT_ETC)/parameter-framework/AudioParameterFramework.xml
ifeq ($(BOARD_USES_MODEM_CENTRIC_ARCHITECTURE),true)
PFW_CRITERIA_FILE := $(COMMON_PFW_CONFIG_PATH)/AudioCriteriaModemCentric.txt
else
PFW_CRITERIA_FILE := $(COMMON_PFW_CONFIG_PATH)/AudioCriteria.txt
endif
ifeq ($(BOARD_USES_MODEM_CENTRIC_ARCHITECTURE),true)
PFW_TUNING_FILE := $(LOCAL_PATH)/Settings/Audio/AudioConfigurableDomains_modem_centric-scalpe.xml
else
PFW_TUNING_FILE := $(LOCAL_PATH)/Settings/Audio/AudioConfigurableDomains-Tuning.xml
endif
ifeq ($(BOARD_USES_MODEM_CENTRIC_ARCHITECTURE),true)
PFW_EDD_FILES := \
        $(PLATFORM_PFW_CONFIG_PATH)/Settings/Audio/routing_imc_modem_centric.pfw \
        $(COMMON_PFW_CONFIG_PATH)/Settings/Audio/routing_realtek5672.pfw \
        $(PLATFORM_PFW_CONFIG_PATH)/Settings/Audio/routing_sst.pfw
else
PFW_EDD_FILES := \
        $(PLATFORM_PFW_CONFIG_PATH)/Settings/Audio/routing_imc.pfw \
        $(COMMON_PFW_CONFIG_PATH)/Settings/Audio/RoutingVoiceProcessingLock.pfw \
        $(COMMON_PFW_CONFIG_PATH)/Settings/Audio/routing_realtek5672.pfw \
        $(PLATFORM_PFW_CONFIG_PATH)/Settings/Audio/routing_sst.pfw \
        $(COMMON_PFW_CONFIG_PATH)/Settings/Audio/routing_power.pfw
endif
include $(BUILD_PFW_SETTINGS)

# phony target for building all generated parameter-framework settings files
# Useful with pfw_rebuild_settings=true
.PHONY: all-audio-settings
all-audio-settings: AudioConfigurableDomains.xml
all-audio-settings: RouteConfigurableDomains.xml
##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := AudioClass.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Audio
LOCAL_SRC_FILES := Structure/Audio/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################

include $(CLEAR_VARS)
LOCAL_MODULE := SstSubsystem.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := parameter-framework/Structure/Audio
LOCAL_SRC_FILES := Structure/Audio/$(LOCAL_MODULE)
LOCAL_REQUIRED_MODULES := libtinyalsa_custom-subsystem
include $(BUILD_PREBUILT)
