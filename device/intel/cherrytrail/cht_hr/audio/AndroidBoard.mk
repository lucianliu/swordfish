# Audio Parameter framework Structure/Settings files and dependancies
include $(TARGET_DEVICE_DIR)/audio/parameter-framework/AndroidBoard.mk

# mamgr (Modem Audio Manager)
include device/intel/common/mamgr/xmm_single_modem_single_sim.mk

LOCAL_PATH := device/intel/cherrytrail/$(TARGET_DEVICE)/audio

##################################################
include $(CLEAR_VARS)
LOCAL_MODULE := topology.audio.$(TARGET_DEVICE)
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES := \
    layout_types.xml \
    layout_topology.xml \
    layout_ui.xml \
    layout_probes.xml
include $(BUILD_PHONY_PACKAGE)

##################################################
include $(CLEAR_VARS)
LOCAL_MODULE := layout_types.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := tuning/audio
LOCAL_SRC_FILES := topology/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := layout_topology.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := tuning/audio
LOCAL_SRC_FILES := topology/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := layout_ui.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := tuning/audio
LOCAL_SRC_FILES := topology/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := layout_probes.xml
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_RELATIVE_PATH := tuning/audio
LOCAL_SRC_FILES := topology/$(LOCAL_MODULE)
include $(BUILD_PREBUILT)

##################################################
# Audio policy files

include $(CLEAR_VARS)
LOCAL_MODULE := audio_policy.conf
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)
##################################################
# Audio Effect conf file

include $(CLEAR_VARS)
LOCAL_MODULE := audio_effects.conf
LOCAL_MODULE_TAGS := optional
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)
##################################################
# The audio meta package

include $(CLEAR_VARS)
LOCAL_MODULE := meta.package.audio
LOCAL_MODULE_TAGS := optional
LOCAL_REQUIRED_MODULES := \
    audio_policy.conf \
    audio_effects.conf \
    audio_hal_configurable \
    parameter-framework.route.cherrytrail \
    parameter-framework.audio.$(TARGET_DEVICE) \
    topology.audio.$(TARGET_DEVICE) \
    audio.hdmi.$(TARGET_BOARD_PLATFORM) \
    audio.r_submix.default \
    audio.r_submix.$(TARGET_PRODUCT) \
    audio.usb.default \
    audio.codec_offload.$(TARGET_PRODUCT)
include $(BUILD_PHONY_PACKAGE)
##################################################
