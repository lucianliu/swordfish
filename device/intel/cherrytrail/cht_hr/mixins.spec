[main]
mixinsdir: device/intel/mixins/groups

[mapping]
product.mk: device.mk

[groups]
boot-arch: efi(uefi_arch=x86_64,fastboot=efi,acpi_permissive=false,use_power_button=true,use_watchdog=true)
config-partition: enabled
kernel: gmin64(path=cht,loglevel=5)
display-density: 280
dalvik-heap: tablet-8.3in-280dpi-2048
csm: true
cpu-arch: slm
houdini: true
bugreport: default
graphics: ufo_gen8
storage: sdcard-mmcblk1-4xUSB-sda-emulated
ethernet: dhcp
camera: ds4
rfkill: true(force_disable=bluetooth)
bluetooth: lightningpeak
gps: cg2k(power=rfkill,trans=ttyHSU1,uart_close=true,uart_pm=true,simu=false,amil=true,amil_if=MDM)
nfc: fdp(use_clk_req=true)
wlan: iwlwifi(chip_id=8000-cht-gmin)
wfa-sigma: sigma_gen
aware: ish
audio: cht-rt5672
media: ufo
usb: host+acc
usb-gadget: g_android
touch: atmel1000
navigationbar: true
device-type: tablet
gms: true
debug-tools: true
factory-scripts: true
sepolicy: intel
widi: gen_sink
charger: true
disk-bus: mmc-cht
thermal: ituxd(modem_zone=true)
serialport: ttyS0
flashfiles: true
debug-logs: true
debug-crashlogd: true
debug-coredump: true
debug-phonedoctor: true
debug-charging: true
debug-mpm: true
lights: true
security: txei
hw-keystore: txei
telephony: tablet_flashless_voice(modems=7260_HW30_SW20 7260_M2,pci_id=14,bus_id=2,ssic_port=5,bus_id_hs=1,usb_port_m2=3,hsic_port=1-0:1.0/port6)
hdcpd: true
hdmi_settings: true
power: true
bcu: true
libhealthd: intel
silentlake: true
libmds: mds
vpp: isv
ccf: enable
sensor-hubs: ish(sensors=accelerometer compass gyroscope light proximity stepcounter stepdetector)
initrc-aosp: override
wov: lpal
widevine: true
debug-lmdump:true
efiprop: used
telephony-config: dynamic(user_build_config=7260_V2_IMS)
intel_prop: true
fota: true
battery: dynamic
videocme: gen
cellcoex: true
ims: bp_centric
passpoint: true
memory: mem-large
libmintel: true
pstore: ram_pram
